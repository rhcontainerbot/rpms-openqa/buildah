%define go_bin go
%define gobuild(o:) GO111MODULE=off %{go_bin} build -buildmode pie -tags=" ${BUILDTAGS:-}" -a -v -x %{?**};

%global with_debug 1

%if 0%{?with_debug}
%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif

%global provider github
%global provider_tld com
%global project containers
%global repo buildah
# https://github.com/containers/buildah
%global import_path %{provider}.%{provider_tld}/%{project}/%{repo}
%global git0 https://%{import_path}

%global built_tag_strip 1.39.1

Name: %{repo}
Version: %{built_tag_strip}
Packager: Podman Debbuild Maintainers <https://github.com/orgs/containers/teams/podman-debbuild-maintainers>
License: ASL-2.0+
Release: 0%{?dist}
Summary: A command line tool used for creating OCI Images
URL: https://%{name}.io
Source: %{git0}/archive/v%{built_tag_strip}.tar.gz
%if "%{_vendor}" == "debbuild"
BuildRequires: go-md2man
BuildRequires: git
BuildRequires: golang
BuildRequires: libassuan-dev
BuildRequires: libbtrfs-dev
BuildRequires: libdevmapper-dev
BuildRequires: libglib2.0-dev
BuildRequires: libgpg-error-dev
BuildRequires: libgpgme-dev
BuildRequires: libseccomp-dev
BuildRequires: libsystemd-dev
BuildRequires: pkg-config
Requires: containers-common >= 4:1
%endif

%description
The %{name} package provides a command line tool which can be used to
* create a working container from scratch
or
* create a working container from an image as a starting point
* mount/umount a working container's root file system for manipulation
* save container's root file system layer to create a new image
* delete a working container or an image

%prep
%autosetup -Sgit -n %{name}-%{built_tag_strip}

%build
export GOPATH=$(pwd)/_build:$(pwd)

mkdir _build
cd _build
mkdir -p src/%{provider}.%{provider_tld}/%{project}
ln -s ../../../../ src/%{import_path}
cd ..

mv vendor src

export CNI_VERSION=`grep '^# github.com/containernetworking/cni ' src/modules.txt | sed 's,.* ,,'`
export LDFLAGS="-X main.buildInfo=`date +%s` -X main.cniVersion=${CNI_VERSION}"

export BUILDTAGS='seccomp exclude_graphdriver_devicemapper'
%gobuild -o bin/%{name} %{import_path}/cmd/%{name}
%gobuild -o bin/imgtype %{import_path}/tests/imgtype
%gobuild -o bin/copy %{import_path}/tests/copy
%gobuild -o bin/tutorial %{import_path}/tests/tutorial
GOMD2MAN=go-md2man %{__make} -C docs

%install
export GOPATH=$(pwd)/_build:$(pwd)
install -D -m0755 bin/%{name} %{buildroot}%{_bindir}/%{name}
install -m 644 -D contrib/completions/bash/%{name} %{buildroot}%{_datadir}/bash-completion/completions/%{name}
install -d %{buildroot}/%{_mandir}/man1
install -m 0644 docs/%{name}*.1 %{buildroot}%{_mandir}/man1
install -m 0644 docs/links/%{name}*.1 %{buildroot}%{_mandir}/man1

install -d -p %{buildroot}/%{_datadir}/%{name}/test/system
cp -pav tests/. %{buildroot}/%{_datadir}/%{name}/test/system
cp bin/imgtype %{buildroot}/%{_bindir}/%{name}-imgtype
cp bin/copy    %{buildroot}/%{_bindir}/%{name}-copy
cp bin/tutorial %{buildroot}/%{_bindir}/%{name}-tutorial

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}*
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}

%changelog
